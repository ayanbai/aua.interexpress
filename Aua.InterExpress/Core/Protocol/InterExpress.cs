﻿using Aua.InterExpress.Core.SSL;
using Aua.InterExpress.Models;
using Aua.InterExpress.Models.Message;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace Aua.InterExpress.Core
{
    public class InterExpress
    {
        private readonly bool _isTest;
        private string Url = Aua.InterExpress.Core.Extensions.IkbUrl();
        private string Certificate = Aua.InterExpress.Core.Extensions.Certificate();
        private readonly Security _security;
        private readonly IexpressDataContext _db;

        public string Transaction { get; private set; }

        public InterExpress(IexpressDataContext db)
        {
            _db = db;
            _isTest = Aua.InterExpress.Core.Extensions.IsDebug();
            _security = new Security(Certificate);
        }

        private XElement Request(XElement data)
        {
            var error = string.Empty;
            var xmlquery = Encoding.GetEncoding(1251).GetBytes("op=gate&r="+data.ToString());
            var url = new Uri(Url);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(RemoteCertificateValidationCallback);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = xmlquery.Length;
                request.Proxy.Credentials = CredentialCache.DefaultCredentials;
                request.ClientCertificates.Add(_security.Create(out error));
                request.CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);

                Stream stream = (Stream)request.GetRequestStream();
                stream.Write(xmlquery, 0, xmlquery.Length);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                var result = XElement.Parse(new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(1251)).ReadToEnd());
                stream.Close();
                return result;
            }
            catch (Exception ex)
            {
                return ExceptionToIE(ex);
            }
        }

        public XElement Check(Requests request, Services service)
        {
            var xml = new XElement("ValidatePayment",
                new XElement("Request",
                    new XElement("TermID", Aua.InterExpress.Core.Extensions.TermId()),
                    new XElement("Password", Aua.InterExpress.Core.Extensions.Password()),
                    new XElement("system_id", Aua.InterExpress.Core.Extensions.SystemId()),
                    new XElement("transit_num", string.IsNullOrWhiteSpace(request.txn_id) ? request.id.ToString() : request.txn_id),
                    new XElement("timestamp", DateTime.Now.ToString("yyyyddMMHHmmss"))),
                new XElement("type_id", service.service_id.Trim()),
                new XElement("Fields",
                    new XElement("sum", request.amount),
                    new XElement("currency", service.currency.Trim()),
                    new XElement(service.service_field.Trim(), request.account)));
            return CheckResponseToAua(xml, request);
        }

        public XElement CheckResponseToAua(XElement ieXml, Requests request)
        {
            string err = string.Empty;
            var xml = Request(ieXml);
            SaveResponse(request.id, xml, ieXml, out err);
            if (!string.IsNullOrWhiteSpace(err))
                return Messages.InternalError(_isTest, "Save response failed");
            var error = xml.Descendants("Error").FirstOrDefault();
            if (error != null)
            {
                var id = xml.Descendants("Id").FirstOrDefault().Value;
                var description = xml.Descendants("Description").FirstOrDefault().Value;
                SaveError(request.id, id, description, out err);
                if (!string.IsNullOrWhiteSpace(err))
                    return Messages.InternalError(_isTest, "Save interexpress error failed");
                switch (id)
                {
                    case "-1": return Messages.InternalError(_isTest, "Неспецифицированная ошибка");
                    case "-2": return Messages.InternalError(_isTest, "Неверный код внешней платежной системы");
                    case "-3": return Messages.InternalError(_isTest, "Внутренняя ошибка конфигурации платежной системы");
                    case "-4": return Messages.InternalError(_isTest, "Неверный номер транзакции");
                    case "-5": return Messages.InternalError(_isTest, "Неверный логин или пароль Терминала");
                    case "-6": return Messages.InternalError(_isTest, "Ошибка заполнения элемента Fields запроса");
                    case "-7": return Messages.NotFound(request.txn_id);
                    case "-8": return Messages.InternalError(_isTest, "Внутренняя ошибка при попытке подтверждения платежа");
                    case "-9": return Messages.InternalError(_isTest, "Неверный транзитный номер платежной транзакции");
                    case "-10": return Messages.InternalError(_isTest, "Ошибка заполнения элемента Fields запроса");
                    case "-12": return Messages.MinAmount(request.txn_id, request.amount.ToString());
                    case "-112": return Messages.NotFound(request.txn_id);
                    case "-1911": return Messages.NotFound(request.txn_id);
                    default: return Messages.InternalError(_isTest, "Unhandled ERROR_ID");
                }
            }
            var transaction = xml.Descendants("transaction").FirstOrDefault();
            if (transaction != null)
            {
                Transaction = transaction.Value;
                Commit(out err);
                return Messages.Check(request.txn_id);
            }
            else
                return Messages.InternalError(_isTest, "Unhandled XML");
        }

        public XElement Pay(Requests request, Services service)
        {
            var xml = new XElement("Payment",
                new XElement("Request",
                    new XElement("transaction", Transaction),
                    new XElement("TermID", Aua.InterExpress.Core.Extensions.TermId()),
                    new XElement("Password", Aua.InterExpress.Core.Extensions.Password()),
                    new XElement("system_id", Aua.InterExpress.Core.Extensions.SystemId()),
                    new XElement("transit_num", request.txn_id),
                    new XElement("timestamp", DateTime.Now.ToString("yyyyddMMHHmmss"))),
                new XElement("type_id", service.service_id.Trim()),
                new XElement("Fields",
                    new XElement("sum", request.amount),
                    new XElement("currency", service.currency.Trim()),
                    new XElement(service.service_field.Trim(), request.account)));
            return PayResponseToAua(xml, request);
        }

        public XElement PayResponseToAua(XElement ieXml, Requests request)
        {
            string err = string.Empty;
            var xml = Request(ieXml);
            SaveResponse(request.id, xml, ieXml, out err);
            if (!string.IsNullOrWhiteSpace(err))
                return Messages.InternalError(_isTest, "Save response failed");
            var error = xml.Descendants("Error").FirstOrDefault();
            if (error != null)
            {
                var id = xml.Descendants("Id").FirstOrDefault().Value;
                var description = xml.Descendants("Description").FirstOrDefault().Value;
                SaveError(request.id, id, description, out err);
                if (!string.IsNullOrWhiteSpace(err))
                    return Messages.InternalError(_isTest, "Save interexpress error failed");
                switch (id)
                {
                    case "-1": return Messages.InternalError(_isTest, "Неспецифицированная ошибка");
                    case "-2": return Messages.InternalError(_isTest, "Неверный код внешней платежной системы");
                    case "-3": return Messages.InternalError(_isTest, "Внутренняя ошибка конфигурации платежной системы");
                    case "-4": return Messages.InternalError(_isTest, "Неверный номер транзакции");
                    case "-5": return Messages.InternalError(_isTest, "Неверный логин или пароль Терминала");
                    case "-6": return Messages.InternalError(_isTest, "Ошибка заполнения элемента Fields запроса");
                    case "-7": return Messages.NotFound(request.txn_id);
                    case "-8": return Messages.InternalError(_isTest, "Внутренняя ошибка при попытке подтверждения платежа");
                    case "-9": return Messages.InternalError(_isTest, "Неверный транзитный номер платежной транзакции");
                    case "-10": return Messages.InternalError(_isTest, "Ошибка заполнения элемента Fields запроса");
                    case "-12": return Messages.MinAmount(request.txn_id, request.amount.ToString());
                    case "-112": return Messages.NotFound(request.txn_id);
                    case "-1911": return Messages.NotFound(request.txn_id);

                    default: return Messages.InternalError(_isTest, "Unhandled ERROR_ID");
                }
            }
            var transaction = xml.Descendants("transaction").FirstOrDefault();
            if (transaction != null)
            {
                var status = xml.Descendants("status").FirstOrDefault();
                if ((status != null) && (status.Value == "BUFFERED"))
                    return Messages.Wait(request.txn_id);
                request.prv_txn = transaction.Value;
                request.result = "0";
                request.comment = "OK";
                Commit(out err);
                return Messages.PaymentOk(request.txn_id, request.prv_txn, request.amount.ToString());
            }
            else
                return Messages.InternalError(_isTest, "Unhandled XML");
        }

        public XElement Balance()
        {
            var xml = new XElement("BalanceRequest",
                new XElement("Request",
                    new XElement("TermID", Aua.InterExpress.Core.Extensions.TermId()),
                    new XElement("Password", Aua.InterExpress.Core.Extensions.Password()),
                    new XElement("system_id", Aua.InterExpress.Core.Extensions.SystemId()),
                new XElement("Details")));
            var response = Request(xml);
            var amount = response.Descendants("amount").FirstOrDefault();
            if (amount == null)
                return Messages.InternalError(_isTest, "Unhandled XML");
            var currency = response.Descendants("currency").FirstOrDefault();
            var timestamp = response.Descendants("timestamp").FirstOrDefault();
            return new XElement("response",
                new XElement("result", 0),
                new XElement("comment", "Операция завершена успешно"),
                new XElement("details",
                    new XElement("amount", amount.Value),
                    new XElement("currency", currency.Value),
                    new XElement("timestamp", timestamp.Value)));
        }

        public bool RemoteCertificateValidationCallback(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private void SaveRequest(long request_id, XElement xml, out string error)
        {
            var response = _db.Responses.First(p => p.request_id == request_id);
            response.request_xml = xml;
            Commit(out error);
        }

        private void SaveResponse(long request_id, XElement responseXml, XElement requestXml, out string error)
        {
            var response = new Responses
            {
                response_xml = responseXml,
                request_xml = requestXml,
                request_id = request_id,
                response_received = DateTime.Now
            };
            _db.Responses.InsertOnSubmit(response);
            Commit(out error);
        }

        private void SaveError(long request_id, string id, string description, out string error)
        {
            var err = new Errors
            {
                error_id = id,
                error_description = description,
                request_id = request_id,
                error_created = DateTime.Now
            };
            _db.Errors.InsertOnSubmit(err);
            Commit(out error);
        }

        private void Commit(out string error)
        {
            error = string.Empty;
            try
            {
                _db.SubmitChanges();
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
        }

        public static XElement ExceptionToIE(Exception ex)
        {
            return new XElement("ValidateResult",
                                new XElement("Error",
                                    new XElement("Id", -10000),
                                    new XElement("Description", "Internal error"),
                                    new XElement("Exception", ex.ToString())));
        }
    }
}
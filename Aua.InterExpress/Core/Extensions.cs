﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;

namespace Aua.InterExpress.Core
{
    public static class Extensions
    {
        public static bool IsMatch(this string str, string pattern)
        {
            return new Regex(pattern, RegexOptions.IgnoreCase).Match(str).Success;
        }

        public static decimal ToDecimal(this string sum, out string error)
        {
            try
            {
                error = string.Empty;
                return Convert.ToDecimal(sum);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return 0;
            }
        }
        public static string BalancePassword()
        {
            try
            {
                return WebConfigurationManager.AppSettings["BalancePassword"];
            }
            catch
            {
                return null;
            }
        }
        public static bool IsDebug()
        {
            var setting = WebConfigurationManager.AppSettings["IsDebug"];
            return setting != null && setting != "0";
        }
        public static string IkbUrl()
        {
            try
            {
                return WebConfigurationManager.AppSettings["Url"];
            }
            catch
            {
                return null;
            }
        }
        public static string Certificate()
        {
            try
            {
                return WebConfigurationManager.AppSettings["Certificate"];
            }
            catch
            {
                return null;
            }
        }
        public static string TermId()
        {
            try
            {
                return WebConfigurationManager.AppSettings["TermId"];
            }
            catch
            {
                return null;
            }
        }
        public static string Password()
        {
            try
            {
                return WebConfigurationManager.AppSettings["Password"];
            }
            catch
            {
                return null;
            }
        }
        public static string SystemId()
        {
            try
            {
                return WebConfigurationManager.AppSettings["SystemId"];
            }
            catch
            {
                return null;
            }
        }
        public static long TestAgent()
        {
            try
            {
                return Convert.ToInt64(WebConfigurationManager.AppSettings["TestAgentId"]);
            }
            catch
            {
                return 3;
            }
        }
    }
}
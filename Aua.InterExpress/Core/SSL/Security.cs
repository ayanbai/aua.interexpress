﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace Aua.InterExpress.Core.SSL
{
    public class Security
    {
        private readonly string _certFile;
        private const string CERTPWD = "InterExpress";

        public Security(string certFile)
        {
            _certFile = certFile;
            //
        }

        public X509Certificate Create(out string error)
        {
            try
            {
                error = string.Empty;
                return new X509Certificate2(_certFile, "InterExpress", X509KeyStorageFlags.MachineKeySet);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return null;
            }
        }
    }
}
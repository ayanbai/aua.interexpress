﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Aua.InterExpress
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Api",
                url: "api/gate/{action}/{service}",
                defaults: new { controller = "Home", action = "Rur", service = "1056" }
            );
            routes.MapRoute(
                name: "Default",
                url: "iexp/{action}",
                defaults: new { controller = "Home", action = "Balance" }
            );
        }
    }
}
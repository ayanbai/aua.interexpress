﻿using System.Web;
using System.Web.Mvc;

namespace Aua.InterExpress
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
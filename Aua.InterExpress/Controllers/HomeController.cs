﻿using Aua.InterExpress.Models;
using Aua.InterExpress.Models.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using Aua.InterExpress.Core;

namespace Aua.InterExpress.Controllers
{
    public class HomeController : Controller
    {
        private readonly bool _isTest;
        private readonly IexpressDataContext _db;

        public HomeController()
        {
            _isTest = Aua.InterExpress.Core.Extensions.IsDebug();
            _db = new IexpressDataContext();
        }

        public XElement Rur(string command, string service, string account, string txn_id, string txn_date, string sum)
        {
            Response.ContentType = "text/xml";
            string error;
            var amount = sum.ToDecimal(out error);
            long agent;
            var agents = _db.Agents.Where(p => p.ip_address == GetIPAddress(Request));
            if (!_isTest)
            {
                if (agents.Count() <= 0)
                    return Messages.InternalError(_isTest, "Agent not found: " + GetIPAddress(Request));
                agent = agents.First().id;
            }
            else agent = Aua.InterExpress.Core.Extensions.TestAgent();
            var services = _db.Services.Where(p => p.service_id == service && p.currency == "rur");
            if (services.Count() <= 0)
                return Messages.InternalError(_isTest, "Services not found");

            var tech = SaveTech();
            if (tech == null)
                return Messages.InternalError(_isTest, "Unknown tech request");
            if (string.IsNullOrWhiteSpace(txn_id)) txn_id = "-1";
            if (string.IsNullOrWhiteSpace(txn_date)) txn_date = DateTime.Now.ToString("ddMMyyyhhmmss");
            var request = SaveRequest(command, account, txn_id, txn_date, amount, agent, tech.id, services.First().id);
            if (request == null)
                return Messages.InternalError(_isTest, "Save request failed");

            if (string.IsNullOrWhiteSpace(error))
            {
                if (request.command == "check")
                {
                    if (!string.IsNullOrWhiteSpace(request.account))
                        return new InterExpress.Core.InterExpress(_db).Check(request, services.First());
                    else return Messages.WrongAbonent(request.txn_id);
                }
                else if (request.command == "pay")
                {
                    var storedRequest = FindStored(request);
                    if (storedRequest != null)
                        return Messages.PaymentStore(storedRequest);
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(request.account))
                        {
                            if (request.amount < services.First().amount_min)
                                return Messages.MinAmount(request.txn_id, request.amount.ToString());
                            else if (request.amount > services.First().amount_max)
                                return Messages.MaxAmount(request.txn_id, request.amount.ToString());
                            else
                            {
                                if (string.IsNullOrWhiteSpace(request.txn_id))
                                    request.txn_id = (_db.Requests.Max(p => p.id) + 1).ToString();
                                var iex = new InterExpress.Core.InterExpress(_db);
                                var check = iex.Check(request, services.First());
                                if (check == null)
                                    return Messages.InternalError(_isTest, "Null response from IKB");
                                var result = check.Descendants("result").FirstOrDefault();
                                if (result == null)
                                    return Messages.InternalError(_isTest, "Null result from IKB");
                                switch (result.Value) { 
                                    case "0": return iex.Pay(request, services.First());
                                    case "1": return Messages.Wait(request.txn_id);
                                    case "5": return Messages.NotFound(request.txn_id);
                                    case "241": return Messages.MinAmount(request.txn_id, request.amount.ToString());
                                    case "300": return Messages.InternalError(_isTest, "Внутренняя ошибка поставщика");
                                    default: return Messages.InternalError(_isTest, "Unknown result from IKB");
                                }
                            }
                        } else return Messages.WrongAbonent(request.txn_id);
                    }
                } else return Messages.InternalError(_isTest, "Incorrect command");
            } else return Messages.InternalError(_isTest, "Incorrect amount: " + error);
        }

        public XElement Kzt(string command, string service, string account, string txn_id, string txn_date, string sum)
        {
            Response.ContentType = "text/xml";
            string error;
            var amount = sum.ToDecimal(out error);

            long agent;
            var agents = _db.Agents.Where(p => p.ip_address == GetIPAddress(Request));
            if (!_isTest)
            {
                if (agents.Count() <= 0)
                    return Messages.InternalError(_isTest, "Agent not found: " + GetIPAddress(Request));
                agent = agents.First().id;
            }
            else agent = Aua.InterExpress.Core.Extensions.TestAgent();

            var services = _db.Services.Where(p => p.service_id == service && p.currency == "kzt");
            if (services.Count() <= 0)
                return Messages.InternalError(_isTest, "Services not found");

            var tech = SaveTech();
            if (tech == null)
                return Messages.InternalError(_isTest, "Unknown tech request");
            if (string.IsNullOrWhiteSpace(txn_id)) txn_id = "-1";
            if (string.IsNullOrWhiteSpace(txn_date)) txn_date = DateTime.Now.ToString("ddMMyyyhhmmss");
            var request = SaveRequest(command, account, txn_id, txn_date, amount, agent, tech.id, services.First().id);
            if (request == null)
                return Messages.InternalError(_isTest, "Save request failed");

            if (string.IsNullOrWhiteSpace(error))
            {
                if (request.command == "check")
                {
                    if (!string.IsNullOrWhiteSpace(request.account))
                        return new InterExpress.Core.InterExpress(_db).Check(request, services.First());
                    else return Messages.WrongAbonent(request.txn_id);
                }
                else if (request.command == "pay")
                {
                    var storedRequest = FindStored(request);
                    if (storedRequest != null)
                        return Messages.PaymentStore(storedRequest);
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(request.account))
                        {
                            if (request.amount < services.First().amount_min)
                                return Messages.MinAmount(request.txn_id, request.amount.ToString());
                            else if (request.amount > services.First().amount_max)
                                return Messages.MaxAmount(request.txn_id, request.amount.ToString());
                            else
                            {
                                if (string.IsNullOrWhiteSpace(request.txn_id))
                                    request.txn_id = (_db.Requests.Max(p => p.id) + 1).ToString();
                                var iex = new InterExpress.Core.InterExpress(_db);
                                var check = iex.Check(request, services.First());
                                if (check == null)
                                    return Messages.InternalError(_isTest, "Null response from IKB");
                                var result = check.Descendants("result").FirstOrDefault();
                                if (result == null)
                                    return Messages.InternalError(_isTest, "Null result from IKB");
                                switch (result.Value)
                                {
                                    case "0": return iex.Pay(request, services.First());
                                    case "1": return Messages.Wait(request.txn_id);
                                    case "5": return Messages.NotFound(request.txn_id);
                                    case "241": return Messages.MinAmount(request.txn_id, request.amount.ToString());
                                    case "300": return Messages.InternalError(_isTest, "Внутренняя ошибка поставщика");
                                    default: return Messages.InternalError(_isTest, "Unknown result from IKB");
                                }
                            }
                        } else return Messages.WrongAbonent(request.txn_id);
                    }
                } else return Messages.InternalError(_isTest, "Incorrect command");
            } else return Messages.InternalError(_isTest, "Incorrect amount: " + error);
        }

        public XElement Balance()
        {
            Response.ContentType = "text/xml";
            //long agent;
            var agents = _db.Agents.Where(p => p.ip_address == GetIPAddress(Request));
            if (!_isTest)
                if (agents.Count() <= 0)
                    return Messages.InternalError(_isTest, "Agent not found: " + GetIPAddress(Request));
            /*if (string.IsNullOrWhiteSpace(key))
                return Messages.Wait("-1");
            if (Aua.InterExpress.Core.Extensions.BalancePassword() != key)
                return Messages.Wait("-1");*/
            return new InterExpress.Core.InterExpress(_db).Balance();
        }

        private static string GetIPAddress(HttpRequestBase request)
        {
            string ip;
            try
            {
                ip = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (!string.IsNullOrEmpty(ip))
                {
                    if (ip.IndexOf(",") > 0)
                    {
                        string[] ipRange = ip.Split(',');
                        int le = ipRange.Length - 1;
                        ip = ipRange[le];
                    }
                }
                else
                {
                    ip = request.UserHostAddress;
                }
            }
            catch { ip = null; }

            return ip;
        }

        private TechRequests SaveTech()
        {
            if (string.IsNullOrWhiteSpace(Request.QueryString.ToString()))
                return null;
            string error;
            var tech = new TechRequests
            {
                request_datetime = DateTime.Now,
                request_ip = GetIPAddress(Request),
                request_query = Request.QueryString.ToString()
            };
            _db.TechRequests.InsertOnSubmit(tech);
            Commit(out error);
            return string.IsNullOrWhiteSpace(error) 
                ? tech
                : null;
        }

        private Requests SaveRequest(string command, 
            string account, 
            string txn_id, 
            string txn_date, 
            decimal amount,
            long agentid,
            long techid,
            long serviceid)
        {
            string error;
            var request = new Requests
            {
                account = account,
                agent_id = agentid,
                amount = amount,
                command = command,
                txn_date = txn_date,
                txn_id = txn_id,
                techrequest_id = techid,
                service_id = serviceid,
                date_created = DateTime.Now,
            };
            _db.Requests.InsertOnSubmit(request);
            Commit(out error);
            return string.IsNullOrWhiteSpace(error)
                ? request
                : null;
        }

        private Requests FindStored(Requests request)
        {
            try
            {
                return _db.Requests
                        .Where(p => p.txn_id == request.txn_id)
                        .Where(p => p.txn_date == request.txn_date)
                        .Where(p => p.account == request.account)
                        .Where(p => p.command.Trim() == "pay")
                        .Where(p => p.prv_txn != null)
                        .First();
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        private void Commit(out string error)
        {
            error = string.Empty;
            try
            {
                _db.SubmitChanges();
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Aua.InterExpress.Core;

namespace Aua.InterExpress.Models.Message
{
    public class Messages
    {
        public static XElement Check(string txnid)
        {
            return CheckXml(txnid, "0", "Абонент существует");
        }

        public static XElement Balance(string sum, string currency, string date)
        {
            return CheckXml("0", "0", string.Format("SUM: {0}, CURRENCY: {1}, DATE: {2}", sum, currency, date));
        }

        public static XElement PaymentOk(string txnid, string prvtxn, string sum)
        {
            return Payment(txnid, prvtxn, sum, "0", "OK");
        }

        public static XElement PaymentStore(Requests request)
        {
            return Payment(request.txn_id, request.prv_txn, request.amount.ToString(), request.result, request.comment);
        }

        public static XElement WrongAbonent(string txnid)
        {
            return CheckXml(txnid, "4", "Неверный формат идентификатора абонента");
        }

        public static XElement Wait(string txnid)
        {
            return CheckXml(txnid, "1", "Ожидайте завершения транзакции");
        }

        public static XElement NotFound(string txnid)
        {
            return CheckXml(txnid, "5", "Неверные реквизиты платежа; абонент не найден");
        }

        public static XElement TechForbidden()
        {
            return Error("8", "Прием платежа запрещен по техническим причинам");
        }

        public static XElement MinAmount(string txnid, string sum)
        {
            return PaymentError(txnid, sum, "241", "Неверная сумма платежа");
        }

        public static XElement MaxAmount(string txnid, string sum)
        {
            return PaymentError(txnid, sum, "242", "Сумма слишком велика");
        }

        public static XElement InternalError(bool isTest, string error)
        {
            return isTest
                ? DetailError("300", "Внутренняя ошибка поставщика", error)
                : Error("300", "Внутренняя ошибка поставщика");
        }

        private static XElement CheckXml(string txnid, string result, string comment)
        {
            return new XElement("response",
                txnid == "-1" ? null : new XElement("txn_id", txnid),
                new XElement("result", result),
                new XElement("comment", comment));
        }

        private static XElement Error(string result, string comment)
        {
            return new XElement("response",
                new XElement("result", result),
                new XElement("comment", comment));
        }

        private static XElement DetailError(string result, string comment, string detail)
        {
            return new XElement("response",
                new XElement("result", result),
                new XElement("comment", comment),
                new XElement("detail", detail));
        }

        private static XElement Payment(string txnid, string prvtxn, string sum, string result, string comment)
        {
            string error;
            return new XElement("response",
                new XElement("txn_id", txnid),
                new XElement("prv_txn", prvtxn),
                new XElement("sum", sum.ToDecimal(out error).ToString("F").Replace(',', '.')),
                new XElement("result", result),
                new XElement("comment", comment));
        }

        private static XElement PaymentError(string txnid, string sum, string result, string comment)
        {
            return new XElement("response",
                new XElement("txn_id", txnid),
                new XElement("sum", sum),
                new XElement("result", result),
                new XElement("comment", comment));
        }
    }
}
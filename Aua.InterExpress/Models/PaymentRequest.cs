﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aua.InterExpress.Models
{
    public class PaymentRequest
    {
        public string Command { get; set; }
        public string TxnId { get; set; }
        public string TxnDate { get; set; }
        public string Account { get; set; }
        public decimal Sum { get; set; }
    }
}